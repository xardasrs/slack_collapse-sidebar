# Slack_collapse sidebar



## Description
Slack has introduced a new sidebar that cannot be removed.
This takes up a lot of unnecessary space in small windows.
This scripts will add a button to collapse the sidebar.


## Installation
The following is a solution with a Chrome extension, where we add a button and transitions with custom CSS/JS:
(This solution can also work for Edge or other browsers if you use a "custom CSS" extension there.)

1. Install an extension in Chrome that allows CSS and JavaScript (JS) changes. (Use for example.
https://chromewebstore.google.com/detail/custom-cssjs-injector-on/gamgadbdliolbhjdcfjjpjfjhgfnckbp)
2. Go to slack.com and open the extension in the browser menu.
3. Paste the script texts into the respective CSS and JS fields and save.
4. Reload the Slack page.

## Usage
You will now find a button in the upper left corner that allows you to expand and collapse the sidebar.

The sidebar is collapsed by default.
But this can be changed by a modification in the extension.
Go to the extension and change the value "const standard = 0;" to "const standard = 1;" in the JS window.