// standard: open = 1; closed = 0
const standard = 0;


/*--------  Do not edit from here  --------*/

window.onload = function() {
	var btn = document.getElementsByClassName("p-ia4_top_nav__container_wrapper");
	/*var test = document.getElementsByClassName("p-ia4_top_nav__container_wrapper");*/
	var sidebar  = document.getElementsByClassName("p-tab_rail");
	var ctl_strip  = document.getElementsByClassName("p-control_strip");
	var space  = document.getElementsByClassName("p-client_workspace");
	var arrow_in = '"←"';
	var arrow_out = '"→"';
	const arrow = document.querySelector(':root');
	
	function getStatus(){
		return getComputedStyle(document.documentElement).getPropertyValue('--arrows');
	}
	
	if (getStatus() == arrow_out){
		document.documentElement.style.setProperty('--arrows', arrow_in);
	}
	
	if (standard == 0){
		setIn();
	} else {
		setOut();
	}
	
	// Create a function for setting a variable value
	function setBtn(){
		if(getStatus() == arrow_in){
		    setIn();
		} else if(getStatus() == arrow_out){
		    setOut();
		}
		
		
		/*if(btn[0].innerHTML == arrow_in){
		    setIn();
		} else if(btn[0].innerHTML == arrow_out){
		    setOut();
		}*/
	}
	
	function setOut() {
		document.documentElement.style.setProperty('--arrows', arrow_in);
		sidebar[0].setAttribute('id', 'sf-show');
		ctl_strip[0].setAttribute('id', 'sf-show');
		space[0].setAttribute('id', 'sf-normal');
	}
	
	function setIn() {
		document.documentElement.style.setProperty('--arrows', arrow_out);
		sidebar[0].setAttribute('id', 'sf-hidden');
		ctl_strip[0].setAttribute('id', 'sf-hidden');
		space[0].setAttribute('id', 'sf-spread');
	}
	
	for(var i=0;i<btn.length;i++){
	    btn[i].addEventListener('click', setBtn, false);
	}
}
